package com.itb.kruger.ui.camera;

import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.widget.Spinner;

import com.itb.kruger.repository.Repository;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class CameraViewModel extends AndroidViewModel {
    Repository repository;

    public CameraViewModel(@NonNull Application application) {
        super(application);
        repository = new Repository(application);
    }

    public void dipatchPhoto(Fragment fragment, int takePhotoCode){
        repository.dispatchTakePictureIntent(fragment, takePhotoCode);
    }

    public void putFileWithMetadata(Location location, Spinner spinner, Context context, ProgressDialog pd){
        repository.putFileWithMetadata(location,spinner,context, pd);
    }

    public String getPhotoPath(){
        return repository.getPhotoPath();
    }
}