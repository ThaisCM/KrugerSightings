package com.itb.kruger.ui.home;

import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import com.itb.kruger.model.*;


import com.itb.kruger.repository.Repository;

import java.util.List;

public class HomeViewModel extends AndroidViewModel {

    private Repository repository;

    public HomeViewModel(@NonNull Application application) {
        super(application);
        repository = new Repository(application);
    }

    public LiveData<List<Photo>> getAllPhotos(ProgressDialog pd){
        return repository.getAllPhotos(pd);
    }

    public LiveData<Boolean> checkPermissions(Activity activity, int requestCode){
        return repository.checkAndRequestPermissions(activity, requestCode);
    }

}