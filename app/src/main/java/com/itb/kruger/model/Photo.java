package com.itb.kruger.model;

import android.net.Uri;

public class Photo {
    private Uri uri;
    private String animal;
    private double latitude;
    private double longitude;

    public Photo() {
    }

    public Photo(Uri uri, String animal, double latitude, double longitude) {
        this.uri = uri;
        this.animal = animal;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getAnimal() {
        return animal;
    }

    public void setAnimal(String animal) {
        this.animal = animal;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "Photo{" +
                "uri=" + uri +
                ", animal='" + animal + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
